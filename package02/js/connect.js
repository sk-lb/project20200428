/**
 * 通信中処理
 */
jQuery(document).ready(function(){

  /**
   * 登録中画面制御処理
   */
  $(".submit-btn").on("click", function(){

    // 通信中画面表示
    $("#connect").css("display", "-webkit-box");
    $("#connect").css("display", "flex");
  });
});
