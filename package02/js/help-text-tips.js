/**
 * helpテキストのアコーディオン処理
 */
jQuery(document).ready(function(){

  /**
   * クライアント名
   */
  $(".help-tip-btn-client").on("click", function(){
    setToggle("client");
  });

  /**
   * 分類1 (支店名など)
   */
  $(".help-tip-btn-category1").on("click", function(){
    setToggle("category1");
  });

  /**
   * 分類2 (部署名など)
   */
  $(".help-tip-btn-category2").on("click", function(){
    setToggle("category2");
  });

  /**
   * キーワード等
   */
  $(".help-tip-btn-keyword").on("click", function(){
    setToggle("keyword");
  });

  /**
   * 営業担当
   */
  $(".help-tip-btn-saler").on("click", function(){
    setToggle("saler");
  });

  /**
   * 担当運用
   */
  $(".help-tip-btn-manager").on("click", function(){
    setToggle("manager");
  });

  /**
   * ランク
   */
  $(".help-tip-btn-rank").on("click", function(){
    setToggle("rank");
  });

  /**
   * 登録日
   */
  $(".help-tip-btn-insert-date").on("click", function(){
    setToggle("insert-date");
  });

  /**
   * 契約日
   */
  $(".help-tip-btn-cont-date").on("click", function(){
    setToggle("cont-date");
  });

  /**
   * 媒体・商材名
   */
  $(".help-tip-btn-media-name").on("click", function(){
    setToggle("media-name");
  });

  /**
   * 依頼内容
   */
  $(".help-tip-btn-order-detail").on("click", function(){
    setToggle("order-detail");
  });

  /**
   * 製作依頼方法
   */
  $(".help-tip-btn-order-method").on("click", function(){
    setToggle("order-method");
  });

  /**
   * 料金プラン
   */
  $(".help-tip-btn-price-plan").on("click", function(){
    setToggle("price-plan");
  });

  /**
   * 依頼上限回数（主原稿）
   */
  $(".help-tip-btn-order-max-count-main").on("click", function(){
    setToggle("order-max-count-main");
  });

  /**
   * 依頼上限回数（複製）
   */
  $(".help-tip-btn-order-max-count-duplicate").on("click", function(){
    setToggle("order-max-count-duplicate");
  });

  /**
   * 最大依頼回数
   */
  $(".help-tip-btn-order-max-count").on("click", function(){
    setToggle("order-max-count ");
  });

  /**
   * 契約
   */
  $(".help-tip-btn-contract").on("click", function(){
    setToggle("contract");
  });

  /**
   * 公開予定日
   */
  $(".help-tip-btn-release-date").on("click", function(){
    setToggle("release-date");
  });

  /**
   * 依頼件数（主原稿）新規（公開初月）<1A> 定型l入力シート(Exce)で依頼
   */
  $(".help-tip-btn-order-count-main1").on("click", function(){
    setToggle("order-count-main1");
  });

  /**
   * 依頼件数（月20件まで）新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-order-count-main2").on("click", function(){
    setToggle("order-count-main2");
  });

  /**
   * 依頼件数（月20件まで）新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する
   */
  $(".help-tip-btn-order-count-main3").on("click", function(){
    setToggle("order-count-main3");
  });

  /**
   * 依頼件数（複製原稿）新規（公開初月）<1A> 定型l入力シート(Exce)で依頼
   */
  $(".help-tip-btn-order-count-duplicate1").on("click", function(){
    setToggle("order-count-duplicate1");
  });

  /**
   * 依頼件数（月80件まで）新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-order-count-duplicate2").on("click", function(){
    setToggle("order-count-duplicate2");
  });

  /**
   * 依頼件数（月80件まで）新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する
   */
  $(".help-tip-btn-order-count-duplicate3").on("click", function(){
    setToggle("order-count-duplicate3");
  });

  /**
   * 主原稿入力シート（Excel）新規（公開初月）<1A> 定型l入力シート(Exce)で依頼
   */
  $(".help-tip-btn-upload-excel-main1").on("click", function(){
    setToggle("upload-excel-main1");
  });

  /**
   * 主原稿入力シート（Excel）新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する
   */
  $(".help-tip-btn-upload-excel-main2").on("click", function(){
    setToggle("upload-excel-main2");
  });

  /**
   * 複製原稿　職種名指示（Excel）新規（公開初月）<1A> 定型l入力シート(Exce)で依頼
   */
  $(".help-tip-btn-upload-excel-duplicate1").on("click", function(){
    setToggle("upload-excel-duplicate1");
  });

  /**
   * 複製原稿　職種名指示（Excel）新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-upload-excel-duplicate2").on("click", function(){
    setToggle("upload-excel-duplicate2");
  });

  /**
   * 複製原稿　職種名指示（Excel）新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-upload-excel-duplicate3").on("click", function(){
    setToggle("upload-excel-duplicate3");
  });

  /**
   * 複製原稿　職種名指示（Excel）新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する
   */
  $(".help-tip-btn-upload-excel-duplicate4").on("click", function(){
    setToggle("upload-excel-duplicate4");
  });

  /**
   * 備考・メモ・注意書き(依頼画面以外)
   */
  $(".help-tip-btn-note").on("click", function(){
    setToggle("note");
  });

  /**
   * 備考・メモ・注意書き 新規（公開初月）<1A> 定型l入力シート(Exce)で依頼
   */
  $(".help-tip-btn-note1").on("click", function(){
    setToggle("note1");
  });

  /**
   * 備考・メモ・注意書き 新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-note2").on("click", function(){
    setToggle("note2");
  });

  /**
   * 備考・メモ・注意書き 新規（公開初月）<1B> 制作4課で手入力
   */
  $(".help-tip-btn-note3").on("click", function(){
    setToggle("note3");
  });

  /**
   * 備考・メモ・注意書き 新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する
   */
  $(".help-tip-btn-note4").on("click", function(){
    setToggle("note4");
  });

  /**
   * 備考・メモ・注意書き 新規（公開初月）<1C> CSV (RPA・マクロ)で依頼する 複製原稿の制作依頼
   */
  $(".help-tip-btn-note5").on("click", function(){
    setToggle("note5");
  });

  /**
   * JSON出力(依頼画面以外)
   */
  $(".help-tip-btn-json-output").on("click", function(){
    setToggle("json-output");
  });

  /**
   * JSON入力(依頼画面以外)
   */
  $(".help-tip-btn-json-input").on("click", function(){
    setToggle("json-input");
  });

  /**
   * アコーディオンの開閉処理
   * @param e 
   */
   function setToggle(e){

    if ($(".help-text-tip-" + e).hasClass("help-text-tip-close")) {

      $(".help-text-tip-" + e).addClass("help-text-tip-open").fadeIn( 1000 );
      $(".help-text-tip-" + e).removeClass("help-text-tip-close");
    } else {

      $(".help-text-tip-" + e).addClass("help-text-tip-close").fadeOut( 1000 );
      $(".help-text-tip-" + e).removeClass("help-text-tip-open");
    }

   }

 });