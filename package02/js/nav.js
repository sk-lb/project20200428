/**
 * ナビ処理
 */
jQuery(document).ready(function(){

  /**
   * ハンバーガーメニュー
   */
  $(".hbgr-btn").on("click", function(){

    if ($(this).hasClass(".hbgr-open")) {

      $(this).removeClass(".hbgr-open");
      $(".hbgr-close").css("display", "none");
      $(".hbgr").css("display", "inline-block");

      $(".nav-title").css("display", "none");
      $(".logout-btn").css("display", "none");
      $(".help-btn").css("display", "none");
      $(".nav").css("height", "50px");
    } else {

      $(this).addClass(".hbgr-open");
      $(".hbgr-close").css("display", "inline-block");
      $(".hbgr").css("display", "none");

      $(".nav-title").css("display", "inline-block");
      $(".logout-btn").css("display", "inline-block");
      $(".help-btn").css("display", "inline-block");

      $(".nav").css("height", "150px");
    }
  });
});