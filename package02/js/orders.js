/**
 * 依頼 レイアウト処理
 */
jQuery(document).ready(function(){

  /** お仕事Noカウントアップ完了値 */
  var addBtnCnt = 1;

  /**
   * 追加するボタン
   */
  $(".add-btn").on("click", function(){

    addBtnCnt++;

    // 親要素取得
    var parentHtml = document.getElementById("works");

    // 新規要素の作成
    var element = document.createElement("div");
    element.className = "input-content";

    // 子要素1(タイトル)の作成
    var elementChild1 = document.createElement("p");
    elementChild1.innerHTML = "HPに掲載のお仕事No." + addBtnCnt;

    // 子要素2(入力フォーム)の作成
    var elementChild2 = document.createElement("input");
    elementChild2.className = "input-text";
    elementChild2.type = "text";
    elementChild2.name = "work" + addBtnCnt;

    // 新規要素に子要素をアペンド
    element.appendChild(elementChild1);
    element.appendChild(elementChild2);

    // 親要素に新規要素をアペンド
    parentHtml.appendChild(element);
  });
});